<?php

/**
 * @file
 * uw_ct_exchange_board.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_exchange_board_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_exchange_board_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_exchange_board_node_info() {
  $items = array(
    'exchange_board' => array(
      'name' => t('Exchange Board'),
      'base' => 'node_content',
      'description' => t('Provides Exchange board functionality (for sale, for free, for rent, or wanted)'),
      'has_title' => '1',
      'title_label' => t('Exchange title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_exchange_board_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: exchange_board
  $schemaorg['node']['exchange_board'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_exchange_email' => array(
      'predicates' => array(),
    ),
    'field_exchange_country' => array(
      'predicates' => array(),
    ),
    'field_exchange_hometown' => array(
      'predicates' => array(),
    ),
    'field_exchange_faculty' => array(
      'predicates' => array(),
    ),
    'field_exchange_academic_year' => array(
      'predicates' => array(),
    ),
    'field_exchange_year_type' => array(
      'predicates' => array(),
    ),
    'field_exchange_program' => array(
      'predicates' => array(),
    ),
    'field_exchange_type' => array(
      'predicates' => array(),
    ),
    'field_exchange_contact_name' => array(
      'predicates' => array(),
    ),
    'field_exchange_contact_info' => array(
      'predicates' => array(),
    ),
    'field_exchange_description' => array(
      'predicates' => array(),
    ),
    'field_exchange_status' => array(
      'predicates' => array(),
    ),
    'field_exchange_photo' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_exchange_permissions' => array(
      'predicates' => array(),
    ),
    'field_exchange_notify' => array(
      'predicates' => array(),
    ),
    'field_exchange_request' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_exchange_full_name' => array(
      'predicates' => array(),
    ),
    'field_exchange_lname' => array(
      'predicates' => array(),
    ),
    'field_exchange_fname' => array(
      'predicates' => array(),
    ),
    'field_exchange_phone' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
