<?php

/**
 * @file
 * uw_ct_exchange_board.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_exchange_board_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_exchange_information|node|exchange_board|form';
  $field_group->group_name = 'group_exchange_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'exchange_board';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Exchange Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_exchange_description',
      1 => 'field_exchange_request',
      2 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_exchange_information|node|exchange_board|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_exchange_photo|node|exchange_board|form';
  $field_group->group_name = 'group_exchange_photo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'exchange_board';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Your Photo',
    'weight' => '3',
    'children' => array(
      0 => 'field_exchange_photo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_exchange_photo|node|exchange_board|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_exchange_status|node|exchange_board|form';
  $field_group->group_name = 'group_exchange_status';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'exchange_board';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Exchange Status',
    'weight' => '5',
    'children' => array(
      0 => 'field_exchange_status',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_exchange_status|node|exchange_board|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_personal_information|node|exchange_board|form';
  $field_group->group_name = 'group_personal_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'exchange_board';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal Information',
    'weight' => '0',
    'children' => array(
      0 => 'field_exchange_email',
      1 => 'field_exchange_lname',
      2 => 'field_exchange_fname',
      3 => 'field_exchange_phone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Personal Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_personal_information|node|exchange_board|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Exchange Information');
  t('Exchange Status');
  t('Personal Information');
  t('Upload Your Photo');

  return $field_groups;
}
