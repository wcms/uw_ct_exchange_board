<?php

/**
 * @file
 * uw_ct_exchange_board.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_exchange_board_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'exchange_board_sidebar';
  $context->description = 'Displays exchange board in the sidebar';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'exchange-board' => 'exchange-board',
        'exchange-board/*' => 'exchange-board/*',
        'exchange-category/rent' => 'exchange-category/rent',
        'exchange-category/wanted' => 'exchange-category/wanted',
        'exchange-category/sale' => 'exchange-category/sale',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-05b35ea1798b19b2470aa79cc3dd6dd4' => array(
          'module' => 'views',
          'delta' => '05b35ea1798b19b2470aa79cc3dd6dd4',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays exchange board in the sidebar');
  $export['exchange_board_sidebar'] = $context;

  return $export;
}
