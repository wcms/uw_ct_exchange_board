<?php

/**
 * @file
 * uw_ct_exchange_board.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_ct_exchange_board_default_rules_configuration() {
  $items = array();
  $items['rules_exchange_board_change_status_approved_'] = entity_import('rules_config', '{ "rules_exchange_board_change_status_approved_" : {
      "LABEL" : "Exchange board change status (approved)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "exchange_board" : "exchange_board" } }
          }
        },
        { "NOT data_is" : {
            "data" : [ "node-unchanged:field-exchange-status" ],
            "value" : "approved"
          }
        },
        { "data_is" : { "data" : [ "node:field-exchange-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_exchange_board_change_status_not_approved_'] = entity_import('rules_config', '{ "rules_exchange_board_change_status_not_approved_" : {
      "LABEL" : "Exchange board change status (not approved)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "exchange_board" : "exchange_board" } }
          }
        },
        { "data_is" : {
            "data" : [ "node-unchanged:field-exchange-status" ],
            "value" : "approved"
          }
        },
        { "NOT data_is" : { "data" : [ "node:field-exchange-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_unpublish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_exchange_board_initial_submit'] = entity_import('rules_config', '{ "rules_exchange_board_initial_submit" : {
      "LABEL" : "Exchange board initial submit",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "exchange_board" : "exchange_board" } }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "exchange-board-submitted" } } ]
    }
  }');
  $items['rules_exchange_board_initial_submit_approved'] = entity_import('rules_config', '{ "rules_exchange_board_initial_submit_approved" : {
      "LABEL" : "Exchange board initial submit approved",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "exchange_board" : "exchange_board" } }
          }
        },
        { "data_is" : { "data" : [ "node:field-exchange-status" ], "value" : "approved" } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_exchange_board_initial_submit_email'] = entity_import('rules_config', '{ "rules_exchange_board_initial_submit_email" : {
      "LABEL" : "Exchange board initial submit email",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "exchange_board" : "exchange_board" } }
          }
        }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "6" : "6" } },
            "subject" : "A new exchange item has been submitted for review",
            "message" : "A new exchange item has been submitted to [site:name]. Please login and review the submission."
          }
        }
      ]
    }
  }');
  $items['rules_exchange_board_published'] = entity_import('rules_config', '{ "rules_exchange_board_published" : {
      "LABEL" : "Exchange board published",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "exchange_board" : "exchange_board" } }
          }
        },
        { "node_is_published" : { "node" : [ "node" ] } },
        { "NOT data_is" : { "data" : [ "node:field-exchange-status" ], "value" : "approved" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-exchange-status" ], "value" : "approved" } }
      ]
    }
  }');
  return $items;
}
