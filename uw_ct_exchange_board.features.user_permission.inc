<?php

/**
 * @file
 * uw_ct_exchange_board.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_exchange_board_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'admin exchange board'.
  $permissions['admin exchange board'] = array(
    'name' => 'admin exchange board',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_exchange_board',
  );

  // Exported permission: 'create exchange_board content'.
  $permissions['create exchange_board content'] = array(
    'name' => 'create exchange_board content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create field_exchange_status'.
  $permissions['create field_exchange_status'] = array(
    'name' => 'create field_exchange_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'delete any exchange_board content'.
  $permissions['delete any exchange_board content'] = array(
    'name' => 'delete any exchange_board content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own exchange_board content'.
  $permissions['delete own exchange_board content'] = array(
    'name' => 'delete own exchange_board content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uwaterloo_exchange_category'.
  $permissions['delete terms in uwaterloo_exchange_category'] = array(
    'name' => 'delete terms in uwaterloo_exchange_category',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any exchange_board content'.
  $permissions['edit any exchange_board content'] = array(
    'name' => 'edit any exchange_board content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_exchange_status'.
  $permissions['edit field_exchange_status'] = array(
    'name' => 'edit field_exchange_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own exchange_board content'.
  $permissions['edit own exchange_board content'] = array(
    'name' => 'edit own exchange_board content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own field_exchange_status'.
  $permissions['edit own field_exchange_status'] = array(
    'name' => 'edit own field_exchange_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit terms in uwaterloo_exchange_category'.
  $permissions['edit terms in uwaterloo_exchange_category'] = array(
    'name' => 'edit terms in uwaterloo_exchange_category',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter exchange_board revision log entry'.
  $permissions['enter exchange_board revision log entry'] = array(
    'name' => 'enter exchange_board revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override exchange_board authored by option'.
  $permissions['override exchange_board authored by option'] = array(
    'name' => 'override exchange_board authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override exchange_board authored on option'.
  $permissions['override exchange_board authored on option'] = array(
    'name' => 'override exchange_board authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override exchange_board promote to front page option'.
  $permissions['override exchange_board promote to front page option'] = array(
    'name' => 'override exchange_board promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override exchange_board published option'.
  $permissions['override exchange_board published option'] = array(
    'name' => 'override exchange_board published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override exchange_board revision option'.
  $permissions['override exchange_board revision option'] = array(
    'name' => 'override exchange_board revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override exchange_board sticky option'.
  $permissions['override exchange_board sticky option'] = array(
    'name' => 'override exchange_board sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view field_exchange_status'.
  $permissions['view field_exchange_status'] = array(
    'name' => 'view field_exchange_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_exchange_status'.
  $permissions['view own field_exchange_status'] = array(
    'name' => 'view own field_exchange_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
