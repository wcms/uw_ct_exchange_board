<?php
/**
 * @file
 * uw_ct_exchange_board.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_exchange_board_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_exchange-board-introductory-text:admin/config/system/exchange_board_header.
  $menu_links['menu-site-management_exchange-board-introductory-text:admin/config/system/exchange_board_header'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/exchange_board_header',
    'router_path' => 'admin/config/system/exchange_board_header',
    'link_title' => 'Exchange Board introductory text',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_exchange-board-introductory-text:admin/config/system/exchange_board_header',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Exchange Board introductory text');

  return $menu_links;
}
